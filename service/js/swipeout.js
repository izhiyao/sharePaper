   +//WEUI  global函数
   (function($) {
     "use strict";

     $.fn.transitionEnd = function(callback) {
       var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
         i, dom = this;

       function fireCallBack(e) {
         /*jshint validthis:true */
         if (e.target !== this) return;
         callback.call(this, e);
         for (i = 0; i < events.length; i++) {
           dom.off(events[i], fireCallBack);
         }
       }
       if (callback) {
         for (i = 0; i < events.length; i++) {
           dom.on(events[i], fireCallBack);
         }
       }
       return this;
     };

     $.support = (function() {
       var support = {
         touch: !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof window.DocumentTouch)
       };
       return support;
     })();

     $.touchEvents = {
       start: $.support.touch ? 'touchstart' : 'mousedown',
       move: $.support.touch ? 'touchmove' : 'mousemove',
       end: $.support.touch ? 'touchend' : 'mouseup'
     };

     $.getTouchPosition = function(e) {
       e = e.originalEvent || e; //jquery wrap the originevent
       if(e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend') {
         return {
           x: e.targetTouches[0].pageX,
           y: e.targetTouches[0].pageY
         };
       } else {
         return {
           x: e.pageX,
           y: e.pageY
         };
       }
     };

     $.fn.scrollHeight = function() {
       return this[0].scrollHeight;
     };

     $.fn.transform = function(transform) {
       for (var i = 0; i < this.length; i++) {
         var elStyle = this[i].style;
         elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
       }
       return this;
     };
     $.fn.transition = function(duration) {
       if (typeof duration !== 'string') {
         duration = duration + 'ms';
       }
       for (var i = 0; i < this.length; i++) {
         var elStyle = this[i].style;
         elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
       }
       return this;
     };

     $.getTranslate = function (el, axis) {
       var matrix, curTransform, curStyle, transformMatrix;

       // automatic axis detection
       if (typeof axis === 'undefined') {
         axis = 'x';
       }

       curStyle = window.getComputedStyle(el, null);
       if (window.WebKitCSSMatrix) {
         // Some old versions of Webkit choke when 'none' is passed; pass
         // empty string instead in this case
         transformMatrix = new WebKitCSSMatrix(curStyle.webkitTransform === 'none' ? '' : curStyle.webkitTransform);
       }
       else {
         transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform  || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
         matrix = transformMatrix.toString().split(',');
       }

       if (axis === 'x') {
         //Latest Chrome and webkits Fix
         if (window.WebKitCSSMatrix)
           curTransform = transformMatrix.m41;
         //Crazy IE10 Matrix
         else if (matrix.length === 16)
           curTransform = parseFloat(matrix[12]);
         //Normal Browsers
         else
           curTransform = parseFloat(matrix[4]);
       }
       if (axis === 'y') {
         //Latest Chrome and webkits Fix
         if (window.WebKitCSSMatrix)
           curTransform = transformMatrix.m42;
         //Crazy IE10 Matrix
         else if (matrix.length === 16)
           curTransform = parseFloat(matrix[13]);
         //Normal Browsers
         else
           curTransform = parseFloat(matrix[5]);
       }

       return curTransform || 0;
     };
     $.requestAnimationFrame = function (callback) {
       if (window.requestAnimationFrame) return window.requestAnimationFrame(callback);
       else if (window.webkitRequestAnimationFrame) return window.webkitRequestAnimationFrame(callback);
       else if (window.mozRequestAnimationFrame) return window.mozRequestAnimationFrame(callback);
       else {
         return window.setTimeout(callback, 1000 / 60);
       }
     };

     $.cancelAnimationFrame = function (id) {
       if (window.cancelAnimationFrame) return window.cancelAnimationFrame(id);
       else if (window.webkitCancelAnimationFrame) return window.webkitCancelAnimationFrame(id);
       else if (window.mozCancelAnimationFrame) return window.mozCancelAnimationFrame(id);
       else {
         return window.clearTimeout(id);
       }
     };

     $.fn.join = function(arg) {
       return this.toArray().join(arg);
     }
   })($);
/* ===============================================================================
************   Swipeout ************
=============================================================================== */
/* global $:true */

+function ($) {
  "use strict";

  var cache = [];
  var TOUCHING = 'swipeout-touching'

  var Swipeout = function(el) {
    this.container = $(el);
    this.mover = this.container.find('>.weui-cell__bd')
    this.attachEvents();
    cache.push(this)
  }

  Swipeout.prototype.touchStart = function(e) {
    var p = $.getTouchPosition(e);
    this.container.addClass(TOUCHING);
    this.start = p;
    this.startX = 0;
    this.startTime = + new Date;
    var transform =  this.mover.css('transform').match(/-?[\d\.]+/g)
    if (transform && transform.length) this.startX = parseInt(transform[4])
    this.diffX = this.diffY = 0;
    this._closeOthers()
    this.limit = this.container.find('>.weui-cell__ft').width() || 68; // 因为有的时候初始化的时候元素是隐藏的（比如在对话框内），所以在touchstart的时候计算宽度而不是初始化的时候
  };

  Swipeout.prototype.touchMove= function(e) {
    if(!this.start) return true;
    var p = $.getTouchPosition(e);
    this.diffX = p.x - this.start.x;
    this.diffY = p.y - this.start.y;
    if (Math.abs(this.diffX) < Math.abs(this.diffY)) { // 说明是上下方向在拖动
      this.close()
      this.start = false
      return true;
    }
    e.preventDefault();
    e.stopPropagation();
    var x = this.diffX + this.startX
    if (x > 0) x = 0;
    if (Math.abs(x) > this.limit) x = - (Math.pow(-(x+this.limit), .7) + this.limit)
    this.mover.css("transform", "translate3d("+x+"px, 0, 0)");
  };
  Swipeout.prototype.touchEnd = function() {
    if (!this.start) return true;
    this.start = false;
    var x = this.diffX + this.startX
    var t = new Date - this.startTime;
    if (this.diffX < -5 && t < 200) { // 向左快速滑动，则打开
      this.open()
    } else if (this.diffX >= 0 && t < 200) { // 向右快速滑动，或者单击,则关闭
      this.close()
    } else if (x > 0 || -x <= this.limit / 2) {
      this.close()
    } else {
      this.open()
    }
  };


  Swipeout.prototype.close = function() {
    this.container.removeClass(TOUCHING);
    this.mover.css("transform", "translate3d(0, 0, 0)");
    this.container.trigger('swipeout-close');
  }

  Swipeout.prototype.open = function() {
    this.container.removeClass(TOUCHING);
    this._closeOthers()
    this.mover.css("transform", "translate3d(" + (-this.limit) + "px, 0, 0)");
    this.container.trigger('swipeout-open');
  }

  Swipeout.prototype.attachEvents = function() {
    var el = this.mover;
    el.on($.touchEvents.start, $.proxy(this.touchStart, this));
    el.on($.touchEvents.move, $.proxy(this.touchMove, this));
    el.on($.touchEvents.end, $.proxy(this.touchEnd, this));
  }
  Swipeout.prototype._closeOthers = function() {
    //close others
    var self = this
    cache.forEach(function (s) {
      if (s !== self) s.close()
    })
  }

  var swipeout = function(el) {
    return new Swipeout(el);
  };

  $.fn.swipeout = function (arg) {
    return this.each(function() {
      var $this = $(this)
      var s = $this.data('swipeout') || swipeout(this);
      $this.data('swipeout', s);

      if (typeof arg === typeof 'a') {
        s[arg]()
      }
    });
  }

  $('.weui-cell_swiped').swipeout() // auto init
}($);